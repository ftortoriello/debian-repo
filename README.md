# debian-repo

Debian 12 (Bookworm) repository containing patched and custom software.

It includes:

- GTK 3 with "Classic" patches.
- Aisleriot with a Spanish deck and a Spanish solitaire variant.
- Synkron updated to use Qt 5.
- Epson Printer Utility built from source and updated to use Qt 5.
- Some LXQt software with improved Spanish translations.
- A web browser extension to play web videos with mpv.
- A minimalistic GUI to enable and disable SSH and OpenVPN.
- Misc fixes and UI simplifications.
