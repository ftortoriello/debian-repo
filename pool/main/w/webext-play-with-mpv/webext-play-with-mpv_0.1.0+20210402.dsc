-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: webext-play-with-mpv
Binary: webext-play-with-mpv
Architecture: all
Version: 0.1.0+20210402
Maintainer: Franco Tortoriello <torto09@gmail.com>
Homepage: http://torto.now-dns.net
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 webext-play-with-mpv deb web optional arch=all
Checksums-Sha1:
 31cdbca34b23638e867bb330c40c48a34e8c9191 16432 webext-play-with-mpv_0.1.0+20210402.tar.xz
Checksums-Sha256:
 7d020e946871a742d819eaa65348668294714f5274ea0453433cc020e3e3bf76 16432 webext-play-with-mpv_0.1.0+20210402.tar.xz
Files:
 6d767afb238c179619a1c5994b64b091 16432 webext-play-with-mpv_0.1.0+20210402.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY7mgSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8YrwL/iBOh81m8i90ZdnU2pqA1ag8O2XIb9jJ
V4we8OYxpIMweESeN0phJ5NcZBKceazUddSPmUcXVu01I0WIYReTFwTGvNTmG7qd
cWpX/sqyvrbpsdHpxtJ+QbrwJRZC8p/rSoGZY4FEdRAkSIuSr20EejRdN607kE6D
8SYL6Ci7ufkybdkUFgWkifb84FLXL54AWhvtW+sXL8qKm1hQ2xo6/MY3SrquEv3n
47jgwEZ4L+5SnB7zncT3uNWm+EmfefqiBxiSE9RDUT6vZF+IS+xxYdyviip9hQ27
eRK5y4S4b+xqL1Z08hlK9BqLoqjqJvq9/D0nWCcKtbSBg2aSuATOPQy4atlSM00H
UX05mBRvYDjpAKXDDMjJVE6+uJR0j/6bla6TR24E2XY2wYm/Yh7hcRusc5uulY2h
FrezSgUS5PYeUpvZSe+3bn1JhHjNleY+tW2mpZHa3tkkcYGtaNZ9YacL8fmal1so
rFFIKUVmvel2zCwJb8BZtpIiWRCGlhF/AQ==
=qj+g
-----END PGP SIGNATURE-----
