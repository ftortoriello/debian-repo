-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: epson-inkjet-printer-201101w
Binary: epson-inkjet-printer-201101w
Architecture: i386 amd64
Version: 1.0.0-1
Maintainer: Franco Tortoriello <torto09@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 epson-inkjet-printer-201101w deb net optional arch=i386,amd64
Checksums-Sha1:
 d8a78a0c1b3d53158ce48579a57c847e681db70f 1863211 epson-inkjet-printer-201101w_1.0.0.orig.tar.gz
 b8da4dc50e13682e4442994f9e8c423515b603df 1260 epson-inkjet-printer-201101w_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 e562f5a09d470d14efb834ecf97cc7b3fa063337b1316139c02771ab26fa5558 1863211 epson-inkjet-printer-201101w_1.0.0.orig.tar.gz
 532c7214fa58184b369c10bf7c466f042d628ba52a0a44a0f5f0110daea8f73f 1260 epson-inkjet-printer-201101w_1.0.0-1.debian.tar.xz
Files:
 c039d50af651ba57aaa47048d82c69e6 1863211 epson-inkjet-printer-201101w_1.0.0.orig.tar.gz
 2e37399a7e382dc7580c420a0952091f 1260 epson-inkjet-printer-201101w_1.0.0-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY2EwSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8KWkL/A0g4ucapWsllEVCZLmxtGJh3OUsCtq7
WbuxtGVW78MWJP9RpGmIGk7G8LG8WneTTXnMtTndj/YmQgYtF7DVnwK6EECobUv9
bziMSqwJPrJFH1ooOZnrt4XY9BF+wmAevmg+LYqRM2eaM2CeqynLOTkEysD4CKaz
UftM5c+8Cu5MJ2uhkXVXc1FfZl6LV2nj5OqzKbBWsJ08becBUusGWkZ+Y310E8CO
4W5kJbTSm11Ohd7lrAdwbdOZr8ZEGZYL0aQpdUsSVHrmrbKxS0sO8N1aaELiqslf
xNlFy/x7AHnB8CLb+YR1dFM2USEsOohmWvYW8DKimZqoj734qLTQSFN+nBZkUEP5
94SV7SfEgLbu5udaLklzyRUyRVMRTddbMTWS1VgH181pcAvyAJJmT59cEhUiwB5v
kCAQdOEejY3KY6Tr1O/y6U02Tbr1+Xo9qJu7X15eU9+EI+xKfo+aDCyMHbnglbaT
yW68GGBMIciUZRgrZ+9zbWAAUpGVi8dDsQ==
=mi0t
-----END PGP SIGNATURE-----
