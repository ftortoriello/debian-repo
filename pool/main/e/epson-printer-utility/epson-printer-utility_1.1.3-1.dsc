-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: epson-printer-utility
Binary: epson-printer-utility
Architecture: i386 amd64
Version: 1.1.3-1
Maintainer: Franco Tortoriello <torto09@gmail.com>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), pkg-config, libcups2-dev, qtbase5-dev
Package-List:
 epson-printer-utility deb net optional arch=i386,amd64
Checksums-Sha1:
 7caef5a7fec048dcf131eda7a15d469fb00ee958 637196 epson-printer-utility_1.1.3.orig.tar.xz
 a18fe02985a68939d07167520f254f457b2d47ed 18348 epson-printer-utility_1.1.3-1.debian.tar.xz
Checksums-Sha256:
 88561451cd1f37c4b63dcdae96c0415fee795fc3424a60ed56cad0ff222c5a6c 637196 epson-printer-utility_1.1.3.orig.tar.xz
 8de780164b0e29e7514c6e42b14ebefa793b52ed3ec4798bcbe8929b2dba3fbc 18348 epson-printer-utility_1.1.3-1.debian.tar.xz
Files:
 6e8545b1d0838ca7232286484ed50b20 637196 epson-printer-utility_1.1.3.orig.tar.xz
 a01b12412c71f2ca2db80a94cd87088f 18348 epson-printer-utility_1.1.3-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY4fgSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8LU4L/A4gWLrjCs/k8dbY1rMGj9DuXrWfzN5Z
6vl9ch5aUouGUSas/Zc14qwuBzS1gzIhLBvwL6vraqcBEwr4xE2570RA+JIYXi6/
6XLZhNZNH+Q5FDB1vkeZFpPqWUiV/HhUuHCx49zE7+vp4WlbcJvwGS/4R2u8Dlxf
n7+OQXHLtP5fio8VgnQT0rNrbq08MSGzbfTMLh7rYl+0EjJ//8yfTCWMCYboTJHv
N2xPPZdQY1ZIyE8qdO0DBArNBydYcLF+Cu7s7vypT2xG5/4jFwY3L5MSNgWxrddz
mMZYPIWahgtWCXmgSXvfGptNyYc9iE0GooRSyHZyaUEj+7nXcgrcAytT+XlIyGmh
IerXqzrtyJ/eXi+wbMy9DooDRmG1vkelieEVG0kuSLj5Cp3N1nSVt1RDIzxpfMhG
/41m//n7p4yR6BR4iLJGX+CPxwN7jWwOxXaClynVu1+lCtb0CjyFSm0oDs9Irirm
n0vmmR0wUOwbyLh623X6ClL6z90RBysKzw==
=XlbN
-----END PGP SIGNATURE-----
