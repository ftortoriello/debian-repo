-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: epson-backend
Binary: epson-backend
Architecture: i386 amd64
Version: 1.3.1-1
Maintainer: Franco Tortoriello <torto09@gmail.com>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), libcups2-dev, libltdl-dev, libusb-1.0-0-dev
Package-List:
 epson-backend deb net optional arch=i386,amd64
Checksums-Sha1:
 7c6ad5fbb11c4bc887c4c2452d4d6d178b2dfa45 62448 epson-backend_1.3.1.orig.tar.xz
 ae5a8b4dd4975c2f7572a118ca955c3a6ae4da86 3068 epson-backend_1.3.1-1.debian.tar.xz
Checksums-Sha256:
 2c02d796bb3815198b094e976928f2bf812507894fa77dbff91024a5858fa36b 62448 epson-backend_1.3.1.orig.tar.xz
 59982b39be6bf71a9722a90b9b35a289ad23872ef16bedd5f62a5c8f5e3a1e1d 3068 epson-backend_1.3.1-1.debian.tar.xz
Files:
 c81872ea16579b51e74e138d43932e56 62448 epson-backend_1.3.1.orig.tar.xz
 5049778ae8a450cf6615c7e0d33d0b79 3068 epson-backend_1.3.1-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY2CISHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ899YMAIBzq9zriwUa9mwaHrt6r3Se0iVHWmPx
nSCHIo46GSjY0E7/s/SdRuiR7R/PybuAG/yRWqMdDSvQcvcZdesGlctyTvdKXF3N
rrYMhjDc50RPdp0xv6kIbhftAvJJLQYPXyscFRO+oM66VbNNUmXTbu0b5oEpYNJE
XKZHiwvTJ5lqfZevLCBjJ8X+OoRDEk/Yj3C+qUcy++PNAmGkaNNq7ovOdUMYuef7
7wF4rkcIZng+Paa++0ZVzoXBaAzlJmcn/g2or456PeJo/cPcDXv5/Wx/+LRQik9b
SZD4Kti7ngrDNAMWQfzfOzBXXkkIZf0Cv+Lu21RV9Mmb8k9juoI/xkFHSBZvkmu5
usyGH1RbQ+eWucIHpluTEl7IFiG3HxFO+MpuUVsHPqthTCrnwthqrgPlgwfhRNbl
LdnBjhzO0DaKz5HwoKGRqv7c5HDEGOAUvKRYSmYWMBupnikvMc9Iar63xl5uTXo3
bhQs9H6CKGigOCdn5XVHAGvdilyCYw9v8g==
=GNZr
-----END PGP SIGNATURE-----
