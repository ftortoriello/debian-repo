-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: epson-inkjet-printer-filter
Binary: epson-inkjet-printer-filter
Architecture: i386 amd64
Version: 1.0.1-1
Maintainer: Franco Tortoriello <torto09@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libcups2-dev, libcupsimage2-dev, libjpeg-dev, libstdc++6
Package-List:
 epson-inkjet-printer-filter deb net optional arch=i386,amd64
Checksums-Sha1:
 88bb3d5e0d344e7d122ed75146b6849a8750bd77 322923 epson-inkjet-printer-filter_1.0.1.orig.tar.gz
 d691e6a0aef3fa4afb0e8e18fe9068f65c3a89c1 2504 epson-inkjet-printer-filter_1.0.1-1.debian.tar.xz
Checksums-Sha256:
 246d83065440582e37039b6f568406d661938a7e737c0031359264a3e85e94ee 322923 epson-inkjet-printer-filter_1.0.1.orig.tar.gz
 7ecd53de02bd41499b4a91b05ba284af84113d07d462974f114b87eb8286ad5c 2504 epson-inkjet-printer-filter_1.0.1-1.debian.tar.xz
Files:
 37286bf56aacffa227a79c5cb2a50495 322923 epson-inkjet-printer-filter_1.0.1.orig.tar.gz
 7425d25f293a90508ab8eb472d2aed17 2504 epson-inkjet-printer-filter_1.0.1-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY2GgSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8BcgL/06bU+rqwDOqZVa6E7XC6p9Td4w8ZsxO
Pm09HYpYFYeETKFWK5reJSrIiFVfRzIRUluVWa8ky1GVZmmkPSjMoMuzriE4HRrV
xtB5YR4i6arLPdahL8D/W/kezPwWfM82jgpivo69Fi0ikYjc1JWPNpEAmNeljUrC
Pvagmiz0kI6Nw3Ab+BixwUElawkEGfrPZQmzgHx1JDyLYI4v5N3fn0YrFMe7y3fT
HFIZSoP2TjNIyajItsOaH5NZyQuuFd9kkJPtFBYu2ZJqCms6GGEiWytwxGNJPwuv
hhgWMN3bXjXgjK7LTsQTZmiuX03SLG+kK+pdvKIDrFWXJodLesKK7OIVgtQt2TG+
pl1TQjXtriL5EUfTpL8L6+YH9zhicanZcaBSX6E9wVKIvAPYHKY0f7v1QVcmQQyN
o69D59gg3ah0/4yZxVEw4XulU1+IwzIXI0nVLLUMpxsNtv3Ja0U8tNZ0AJapgJRF
CHE84grw+HCYDeFC5YqUQXUj4PcT7sOVmw==
=PEJN
-----END PGP SIGNATURE-----
