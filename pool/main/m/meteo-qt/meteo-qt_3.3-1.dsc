-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: meteo-qt
Binary: meteo-qt, meteo-qt-l10n
Architecture: any all
Version: 3.3-1
Maintainer: LXQt Packaging Team <pkg-lxqt-devel@lists.alioth.debian.org>
Uploaders: Alf Gaida <agaida@siduction.org>, ChangZhuo Chen (陳昌倬) <czchen@debian.org>
Homepage: https://github.com/dglent/meteo-qt
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/lxqt-team/meteo-qt
Vcs-Git: https://salsa.debian.org/lxqt-team/meteo-qt.git
Build-Depends: debhelper-compat (= 13), dh-python, python3-all, python3-all-dev, python3-setuptools, pyqt5-dev-tools, qt5-qmake, qtbase5-dev, qttools5-dev, qttools5-dev-tools
Package-List:
 meteo-qt deb python optional arch=any
 meteo-qt-l10n deb localization optional arch=all
Checksums-Sha1:
 097b66477b7325254097170d26acf86569949b1b 931450 meteo-qt_3.3.orig.tar.gz
 b2a1923e510b7581b25ce5d0f16eed874554e0a7 13436 meteo-qt_3.3-1.debian.tar.xz
Checksums-Sha256:
 823647bcc7a6de9379c9608d30c5231257d66fe172e9d96bf3962900dc8e8af5 931450 meteo-qt_3.3.orig.tar.gz
 ac43c76b36729a0d1bbdc5bfc6d07378f500fa2ddefacd7dd2bf365d4f26203f 13436 meteo-qt_3.3-1.debian.tar.xz
Files:
 7d7eb9f85f1784239ad08d9cb9c93f73 931450 meteo-qt_3.3.orig.tar.gz
 22739945cee7c017c0acd245049bf9bf 13436 meteo-qt_3.3-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY56QSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8mhIL/2RqW9aQ4mvAUpMjjakE2wSeDL2Hlgg9
voa1GWi9PKiynUKgHGKgGVRhIZuTz2vgA9mHYCLo3/GBPXkaP67ajsp7MBjN7deP
Lyu656zRs5VT1AD12e8oJbtRFfaOPl4SPVY6I4s1AMKVtY1ELSQAhgFG52waL56U
yoJxUza7oCq1yZmX2pCwCXXOkXppxmzVCDfj+BMCPsCZNqGdU7I79M40QMWsRAfP
eg6BPL1EwD5IGdfuXYA2Ubo9NSqDUL21c0VonaWvmddH5MXqHYLs6wV+XkaL4Ny+
vSL2rXotKBvraPDejZDqYvJgMh5mdG/wJaLu2zctmdqmGqHaR+fK4v23c33osotb
zwcfrkxeuYFW1YlmGLdGMSHLWbZaPIZ1e6+aF6eqZFRScyS3qWHn4wuAfh6HLqh2
BHfirBT93cu+BnMJSznmU4plhXM8xHlyX1n3fNH4ZPvNQFFHAgHjpNv6gnyMjrPR
sGoYX6Xa6pgB5I6vQ2C0WTBLD6/HaybP4A==
=tQCW
-----END PGP SIGNATURE-----
