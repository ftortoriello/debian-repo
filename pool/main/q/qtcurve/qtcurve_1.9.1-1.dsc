-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: qtcurve
Binary: gtk2-engines-qtcurve, kde-style-qtcurve-qt5, qtcurve, libqtcurve-utils2
Architecture: any
Version: 1.9.1-1
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Boris Pek <tehnick@debian.org>
Homepage: https://invent.kde.org/system/qtcurve
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/qtcurve
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/qtcurve.git
Build-Depends: cmake, debhelper-compat (= 13), extra-cmake-modules, gettext, libcairo2-dev, libgtk2.0-dev, libkf5archive-dev, libkf5config-dev, libkf5configwidgets-dev, libkf5guiaddons-dev, libkf5i18n-dev, libkf5iconthemes-dev, libkf5kio-dev, libkf5style-dev, libkf5widgetsaddons-dev, libkf5windowsystem-dev, libkf5xmlgui-dev, libqt5svg5-dev, libqt5x11extras5-dev, libx11-dev, libx11-xcb-dev, libxcb1-dev, pkg-config, pkg-kde-tools, qtbase5-dev, qtbase5-private-dev
Package-List:
 gtk2-engines-qtcurve deb gnome optional arch=any
 kde-style-qtcurve-qt5 deb kde optional arch=any
 libqtcurve-utils2 deb libs optional arch=any
 qtcurve deb metapackages optional arch=any
Checksums-Sha1:
 3158759051770e99beee7079ca948e7074feddd4 800765 qtcurve_1.9.1.orig.tar.gz
 7bb9dd923a8bfd8ee600a935cdcf459f038d4e30 51984 qtcurve_1.9.1-1.debian.tar.xz
Checksums-Sha256:
 fbfdafdac90d4c540dd55a4accfecfc3a17c1f532c5241e28003348beafaca15 800765 qtcurve_1.9.1.orig.tar.gz
 9817ead72f11a6a657e02b025775d5adf69a9d7373cc046f053672a35bd0cf4e 51984 qtcurve_1.9.1-1.debian.tar.xz
Files:
 7ecafe114212a75381b460fe3166c5bd 800765 qtcurve_1.9.1.orig.tar.gz
 e90a76d65e61521de4362c713d1ffe90 51984 qtcurve_1.9.1-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY7xUSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ86iEL/iitMyCPcMJpUoNT7ewM/7HhzNj2KGpj
nJjtUUDM7eiNhSOCsG//2pzfoVFMxDzHFL1uulaFvo56lVw+m1rgUe7ZSCFsNDSk
6j81rFxXDdNPSllGBOGZjNrsQWSeLdpM+KiIq1Y+yVaghHz4Li0gmWvLzheXXI42
hyCFVT1A00QJMFzakoLC8Z6OuUeh41sLjtfYHHjXFBgMWRC88WqtoYlVGbl0KkFk
BEH1SaG3rZzVK6+1AlvWh0lSKp7hZT8nnrlXpxEL1YtfI9rfkfLcriQqLgIbCZVG
Sh5/bpZ4XFrggnk/YujFfePKLypqBbuydGK1oi5CV6KG65R852Iq86Cd6Uvm4VRh
jOUDElDbh62SWit+9qYu0uPpQZCkM5Lsi8JBBgDyNT06i0Fh0yQtDMcYL/VNsPT6
ljCWb0hfHFPENHSlP6Bvt3IO07/WTQjyes17+3P23OxmV4nDMLITRf/ML4Z2YR09
xTe/loEZiGyER1TcErJMsEZjSOA5c+uaHA==
=sBQk
-----END PGP SIGNATURE-----
