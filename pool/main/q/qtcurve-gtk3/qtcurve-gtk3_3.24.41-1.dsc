-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: qtcurve-gtk3
Binary: qtcurve-gtk3
Architecture: all
Version: 3.24.41-1
Maintainer: Franco Tortoriello <torto09@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 qtcurve-gtk3 deb x11 optional arch=all
Checksums-Sha1:
 c1b766a0a3d76cee7533dced0740263dc290a65d 146364 qtcurve-gtk3_3.24.41.orig.tar.xz
 0dd1c252744bb5a473c96ce1224e8eeff0f02e61 1252 qtcurve-gtk3_3.24.41-1.debian.tar.xz
Checksums-Sha256:
 c70baa116c9c5378701e0b40c752e97d1451bef4c2f57fd01ddbfbe5d1ab1514 146364 qtcurve-gtk3_3.24.41.orig.tar.xz
 9e7f9d60200f3fbe68480090681e97c94932f4c077a71a975c365d58b6835f42 1252 qtcurve-gtk3_3.24.41-1.debian.tar.xz
Files:
 af3268cc6db225aaf40f328fdee6af07 146364 qtcurve-gtk3_3.24.41.orig.tar.xz
 555f63cd5cf16cf0c9692e90cba595c6 1252 qtcurve-gtk3_3.24.41-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY8TcSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8pSwL/35i9su+h22U58Zvxt5lcZyqCIqjQEww
fJGLCd/hZGNP9IwJRaR3gHlrGRCHTtRSGMbHHbHZQnWdWzGEa6kOSh4kMiV+3UYL
93ZmwvSpNJa8smRjBfglUgpgkoQBaNt7KKLhAMMWm/28+tvKJHW9NXdGn2kr4riH
WjbTa38S1pEH+hMAsZe/VKSv4TNtTi95jGjpCrnp58tcQjVzDkETREIGYhAOsbMQ
Gl0Q+vSL3J6syimnjT+7xB/BwAd+sqp8e7bpn1DFA5pCdMAYdkM+z/6ABOnEN5yo
BpHcn3lzx2kq+TqyZ/sCbo5wNXS0OeTuR9ODMU8/Sh2BzWA5plVC/RFl6XP+gBrH
MIvQJe9vydC+V5XWQCdyTeoE1tpzObL34CpJGSIpJFZqy7vEwfscQsz/DP2LtwhK
TVadRp13DXSdtMsblq1x3F6Ro57ZNCPOkQQ+L6Ftuf8Z90s0EbV7QLm8VbHLYCEY
MOX7A/y4Ir7PxRamCKe5nRc2vijEJI8AxA==
=gbJv
-----END PGP SIGNATURE-----
