-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: nm-tray
Binary: nm-tray, nm-tray-l10n
Architecture: any all
Version: 0.5.0-1
Maintainer: LXQt Packaging Team <pkg-lxqt-devel@lists.alioth.debian.org>
Uploaders: Alf Gaida <agaida@siduction.org>, ChangZhuo Chen (陳昌倬) <czchen@debian.org>
Homepage: https://github.com/palinek/nm-tray
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/lxqt-team/nm-tray
Vcs-Git: https://salsa.debian.org/lxqt-team/nm-tray.git
Build-Depends: debhelper-compat (= 13), cmake, libkf5networkmanagerqt-dev, modemmanager-qt-dev, qttools5-dev, qttools5-dev-tools
Package-List:
 nm-tray deb net optional arch=any
 nm-tray-l10n deb localization optional arch=all
Checksums-Sha1:
 dd9a76fde827be7be9278fda3e4cef5ae62ebc02 53854 nm-tray_0.5.0.orig.tar.gz
 5f25509efbbc6790d513f867fe5dddf1cfd6f1b6 4172 nm-tray_0.5.0-1.debian.tar.xz
Checksums-Sha256:
 3db9fdfcf274392f040ee2e7370be41aaae5d700277979f661718e6d893c5ff5 53854 nm-tray_0.5.0.orig.tar.gz
 2cca52f98d0b820ce2f21297a3094f12205c05946c9b753b55cf7fd260000675 4172 nm-tray_0.5.0-1.debian.tar.xz
Files:
 8451c44dd252a9a55a36f6a79cecc9af 53854 nm-tray_0.5.0.orig.tar.gz
 6864578d06a7c8dd434ea65ad0d07888 4172 nm-tray_0.5.0-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY6VcSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8RC0L/3qbn0DJLaol/8P76JSKx/BzRrZS6eOh
0Y4KMIOu769EKgmzN/gdIgCBtWzFCAHAPaOJlHVmKZUmYMN80RthV1MWS25tqplW
mnVyxIVf0MycFJUzejzqSyQS1gX7tphw/nXdxzEuNJu2qOkZkHfrZdrJOBVgRS22
mfpoBUqJD8Z4SLMlIpQoPjZx5c6ILOilbnvUwaqvdi7dcw5/ysN1gB1ewQskd6Kz
hGig5s9avb/QHGClKNFxGPQ6eJ3pfRJ868pXR9waVHs0biNheoo9YpQ+Lt4XfRJb
zl3PYV5zNhqKFGlcX+A9/ogQwHdcbnDXgb5kSWOf8xsdyg7EVezEhZVKqf9AUJmQ
10SR8ORCNL3m6/CNISe2ycLzs1TOUiMY7kXDrVAO70goieQOGFXFZW9kNOzF2Rg/
Bjthr8SZ9HTDt1IEbxQcDD1U31+KdCs481Pz8yOEFMFplBHT4Uq9clMCepj1Cbks
POnFqw8WAzHfxDsjkJAt7gdG1LwcgtsHDg==
=wcom
-----END PGP SIGNATURE-----
