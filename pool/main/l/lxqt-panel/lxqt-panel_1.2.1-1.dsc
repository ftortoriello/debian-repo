-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: lxqt-panel
Binary: lxqt-panel, lxqt-panel-l10n
Architecture: any all
Version: 1.2.1-1
Maintainer: LXQt Packaging Team <pkg-lxqt-devel@lists.alioth.debian.org>
Uploaders: Alf Gaida <agaida@siduction.org>, ChangZhuo Chen (陳昌倬) <czchen@debian.org>, Andrew Lee (李健秋) <ajqlee@debian.org>
Homepage: https://github.com/lxqt/lxqt-panel
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/lxqt-team/lxqt-panel
Vcs-Git: https://salsa.debian.org/lxqt-team/lxqt-panel.git
Build-Depends: debhelper-compat (= 13), libasound2-dev, libdbusmenu-qt5-dev, libicu-dev, libkf5windowsystem-dev, libkf5solid-dev, liblxqt1-dev (>= 1.2.0~), liblxqt-globalkeys1-dev (>= 1.2.0~), liblxqt-globalkeys-ui1-dev (>= 1.2.0~), libpulse-dev, libqt5svg5-dev, libqt5x11extras5-dev, libsensors-dev [!hurd-any], libstatgrab-dev [linux-any], libsysstat-qt5-0-dev (>= 0.4.6~), libx11-dev, libxcomposite-dev, libxcb-composite0-dev, libxcb-image0-dev, libxcb-util0-dev, libxcb-xkb-dev, libxcb-damage0-dev, libxcb-randr0-dev, libxdamage-dev, libxkbcommon-dev, libxkbcommon-x11-dev, libxrender-dev, libxtst-dev
Package-List:
 lxqt-panel deb x11 optional arch=any
 lxqt-panel-l10n deb localization optional arch=all
Checksums-Sha1:
 72fda21fb2d0699e3430806630c53c8ddf699e0e 600436 lxqt-panel_1.2.1.orig.tar.xz
 ea2f2d9443d6971074eec4ca8e5136117d596171 10796 lxqt-panel_1.2.1-1.debian.tar.xz
Checksums-Sha256:
 4b380bf7ca00e66b88fc5bb8e23bda0be1ddccc2ea99d3f68e8f86e7c9ca0498 600436 lxqt-panel_1.2.1.orig.tar.xz
 a0121151ede9e82004fb08d09542f67c53d8dab47161d3dffb621319ceb69c5c 10796 lxqt-panel_1.2.1-1.debian.tar.xz
Files:
 74d0f9c6b15da7a192f7eca129f3ca8b 600436 lxqt-panel_1.2.1.orig.tar.xz
 074d79a65f5f0df0dae03f8fbcc793a8 10796 lxqt-panel_1.2.1-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY5owSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8yAoL/jgar9iEhk20BMVZwJq99IjrK78/EHyA
kkyt5acEGmfgXh+x670Rro3cRJM6R0EsPylaCXHz4rvNPWklExGI4kwnha+RfmjR
5bGwSWG0DhNkNHOsHqDJJ/g/qQ4yMJT7HoW7utD6sXQQd4G1hcNXon75VvC67kAS
lVGMo8AAIf+hDKW6cv1JcYCFfDvgpdK8acBesMcTAQbffe5WCQJ2gYQ6JR66hUWU
jLZeak/RgRZUdjakNCkNxNlAmakOdziHs5G3rTID9iktItvka1UJFAMxH9+K4P6/
TxtAc/HvrsjlrTFH+hjc3RLKBBorSzu9Aynt7E0WZ9a4Xx9vGnCAPXZ5jf4kR29S
GpBmFCULNaMdf10Lj1ck0h6vVVs1R3+Y0M/QQQYelUm/lafaAFMwqENSSzwtabMt
fqXSRKP5IMd6PCkqB/Ir2gjVtUGcfTVIwNkRZM9lIBik70xroMOwG++m1nbi1IdZ
RmvAm7fiMPYReLepFEFEhup9DIJyDCh81g==
=sowy
-----END PGP SIGNATURE-----
