-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: aisleriot
Binary: aisleriot, gnome-cards-data
Architecture: any all
Version: 1:3.22.23-1
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Homepage: https://wiki.gnome.org/Apps/Aisleriot
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/gnome-team/aisleriot
Vcs-Git: https://salsa.debian.org/gnome-team/aisleriot.git
Build-Depends: debhelper-compat (= 13), desktop-file-utils, docbook-xml <!nocheck>, lsb-release, meson (>= 0.49.0), guile-3.0-dev, itstool, libcairo2-dev (>= 1.10), libcanberra-gtk3-dev (>= 0.26), gobject-introspection (>= 0.6.3), libgirepository1.0-dev (>= 0.6.3), libglib2.0-dev (>= 2.32.0), libgtk-3-dev (>= 3.18.0), librsvg2-dev (>= 2.32.0), libsm-dev, libxml2-dev, libxml2-utils, pkg-config (>= 0.15)
Package-List:
 aisleriot deb games optional arch=any
 gnome-cards-data deb games optional arch=all
Checksums-Sha1:
 b181a569c03491423fa658656e6dffc70e0a31b4 12222225 aisleriot_3.22.23.orig.tar.bz2
 5ee951d4a7a540bc52ffb5dbe8dadc7963284dc2 4207080 aisleriot_3.22.23-1.debian.tar.xz
Checksums-Sha256:
 69e9c29193eba1622948d0960ad7cd496751d2b1d1ca9de13808d0ce76d4937d 12222225 aisleriot_3.22.23.orig.tar.bz2
 c504d90fc44a00ed3edb75bc0366f6c04e5c17acdfd9375677b544f78dc1e948 4207080 aisleriot_3.22.23-1.debian.tar.xz
Files:
 b3bae634b2b76385eab0c97cf9cbafd0 12222225 aisleriot_3.22.23.orig.tar.bz2
 1d197b602e9a61a766e9155793b5bda9 4207080 aisleriot_3.22.23-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY0f4SHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8MlYL/3j9wjX1UtVFn23qe+0G7UgBV4u8y8vb
4NP9a7e0GCjUzVJlRwY5PuJCBFLOowq7G2Qi2IJocfvJWiQ0oDCVc/ZpGlOzmf4I
TZHSek/BMBB3slTd+jBWpUgm9Jouv/D1GStSTFJbDIBtvoxU20Y+xTDdYu4kTimL
lSsBy5y3XERGf1jLjO+7IxfUnRh0jzbrYzOaTfZ/wXlc0nq0/wM5vYHvJIm+IxhC
eNCTQFTCM7CvWk+jUWwWOo9mz+AJ1GqHXLdDEOEFO4AbgUC6c4sTgdZFhlgAjF1V
wsI52c3zDhLOlbakoOjef3dP3Mo/jl+VhB+bkOXVkMKGjBzMI5/xbOS3JaeQj0Gr
hdzchKSVUJKH+Qjv+DAHuYKbsJK0/j06SkObKQQ/Rf1pmOKRnTiPUyXRcsOU2xkw
LbXglj3xmU1BnxkMbaFjd4EQVtmIkCH9CQJg/H+pDeeEl9VNpuVyNkS0aKvsw2Sm
1Bn6i+EdJ9QxVMd4KW/BiEMPEm4S0zY81Q==
=Z5ts
-----END PGP SIGNATURE-----
