-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: play-with-mpv-server
Binary: play-with-mpv-server
Architecture: all
Version: 0.1.0+20211208
Maintainer: Franco Tortoriello <torto09@gmail.com>
Homepage: https://github.com/Thann/play-with-mpv/
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), dh-python, python3-pkg-resources, python3-setuptools
Build-Depends-Indep: python3
Package-List:
 play-with-mpv-server deb web optional arch=all
Checksums-Sha1:
 b76adb4fdbe6582e56b6f0bfd6e29f704232c804 4720 play-with-mpv-server_0.1.0+20211208.tar.xz
Checksums-Sha256:
 63977784f6ab8d780eba5466a3be20d5eacbcfd56c740d018f529c6d737688ef 4720 play-with-mpv-server_0.1.0+20211208.tar.xz
Files:
 11383ab3810416197014463973298127 4720 play-with-mpv-server_0.1.0+20211208.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXZFAYSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8rO8L/jjKFfM06Tj1RrzQZV0iEVRSnSi6p3Bi
uXMC5YgpkqfGNRRGseaZg/VoCN3HT+J+oXJ+ZKSSaLbLEqoj0MGj6odWhSQ6WeK9
ENZUMbavLXw3cZn4kMslJSsn+UwLKMIoVasnBUr5maFlNS3en7XHAIjud5sXGXEj
k7qcO1z+U3MBAUZXYDkVxN/QqhUQy9COiWgC+ZkR4gbLQ5gSX0UA/FMeV6L0hQiM
da1sidNDX2pYlYT/++7s79BCpr1Tx02x/Gc+qnVoauULRlqWoFg/d/Q9M8t2YjdH
NBjWgru1ITxxs9ClipXNi12Hs4ry+9UgUiEMwU+WXPXbyrG7RL4hMY4hjvhjOMWs
8W62J0srurOwBYltM+LPCB+65wEe2X9qtYnVTdnisVwdjt7jYK7RJmrWzXofHafk
lYOCKxJfoWJf2S4CKM28nD5bMK7JFi/65Qo5f8w5SnI+/TNqsTufgnXMMhPOau+B
1nihcwl/oKirpihFIpNN/TwvvBKme+zZ+w==
=6A3u
-----END PGP SIGNATURE-----
