-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: virtualbox-guest
Binary: virtualbox-guest-x11, virtualbox-guest-utils
Architecture: amd64 i386
Version: 6.1.50-dfsg-1
Maintainer: Debian Virtualbox Team <team+debian-virtualbox@tracker.debian.org>
Uploaders: Ritesh Raj Sarraf <rrs@debian.org>, Gianfranco Costamagna <locutusofborg@debian.org>
Homepage: https://www.virtualbox.org
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/pkg-virtualbox-team/virtualbox
Vcs-Git: https://salsa.debian.org/pkg-virtualbox-team/virtualbox.git
Build-Depends: debhelper-compat (= 13), dpkg-dev (>= 1.15.6~), kbuild (>= 1:0.1.9998svn3572~), liblzf-dev, libssl-dev, libxext-dev, libpam0g-dev, libxmu-dev, libxrandr-dev, lsb-release, module-assistant, xserver-xorg-dev, xsltproc, yasm
Package-List:
 virtualbox-guest-utils deb contrib/misc optional arch=amd64,i386
 virtualbox-guest-x11 deb contrib/x11 optional arch=amd64,i386
Checksums-Sha1:
 bd1497c5593fd1b3e469943e49386449033211a0 20242460 virtualbox-guest_6.1.50-dfsg.orig.tar.xz
 c9f573cf061a55e62abcff918a03a5b7f7f839ed 53656 virtualbox-guest_6.1.50-dfsg-1.debian.tar.xz
Checksums-Sha256:
 fd9069013fc917d35673c447d1add9086c6f845d720c3720356499f3d1937738 20242460 virtualbox-guest_6.1.50-dfsg.orig.tar.xz
 824f8ae896a98513c25ea51149ad56d2ec7606a6df341401b95850a570b20f68 53656 virtualbox-guest_6.1.50-dfsg-1.debian.tar.xz
Files:
 8ceb1d0de6c376c033f060dcf2f2f890 20242460 virtualbox-guest_6.1.50-dfsg.orig.tar.xz
 48c0704f6e30b996808a652827dfc110 53656 virtualbox-guest_6.1.50-dfsg-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXXxYYSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ83MML/2JdiQASd3SRD29g1rGhC0ral7uZ/grX
wyrYYFKBFmZMEl6B/mIyGB9Ln4rIcenxco56ngtcrROeUy4Ug9yvrXMmL9X86Ug0
3x7KKUDi2rCxIWxCVk2xelt1hLaL6bDB13O25LRIH1ddnv70vHd8DzBU6RrMkeUb
3koX2KvXsTjrP6qplQuDMddFyHuVz31OcilSVtA/cl9lBNN16JjVnkunKoJif3x0
y6St90yjH6vcqcIyaFluxweSQNhebJD+I2+SLJBMTt3X88ZArL4k4VTsEip9KLHP
+MTL+2CJ0L8NeoycOxXmw+ux9S2qSzQ/EtD71bcKL/g2LoIvWnQBzv4Ll6pfFTG4
4+lSf7VXIkXd5TB9Atmq39ZYjyGrWGhNvG0k3NXlFxF983D0AGeClViBfHMSVYGw
zJ0R0um5tbUG4tzLFub6bGrcvCuY2SwiAx4Q76assePoAul2Z4GzpaIMv33IkKZr
X2cqIEBm1LiuySheQSVRjH5HmGlF8k+Qvg==
=eZb2
-----END PGP SIGNATURE-----
