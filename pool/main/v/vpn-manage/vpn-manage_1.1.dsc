-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: vpn-manage
Binary: vpn-manage
Architecture: all
Version: 1.1
Maintainer: Franco Tortoriello <torto09@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 vpn-manage deb admin optional arch=all
Checksums-Sha1:
 bbbcbbac8087a9d783ad8e0b5b2e832c8a281c88 2776 vpn-manage_1.1.tar.xz
Checksums-Sha256:
 423106b09c73dc04fcb881d92ecc318e7478de17bc42297c0571fcb6f4fa060a 2776 vpn-manage_1.1.tar.xz
Files:
 e9abab049ec0d6577c9f709f5d2bc6cd 2776 vpn-manage_1.1.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHGBAEBCgAwFiEEqCzjxeu9qHzt0OX41WdRYWXvNnwFAmXY9CcSHHRvcnRvMDlA
Z21haWwuY29tAAoJENVnUWFl7zZ8SIEL/3T0IKb0Oul2sw0iJqOcW8Ee//U2O2dg
7tFzAAyj7wyAgXz9Ja4sm8mGr+OGjJThBjTCVp2V1H1DHP6foMR6Kt8nAPriSR4z
AWeU6fEKFroC3ZXEBJBDwVEre4dRYX3CxS9uNgjLh6ufYfGs5yQ6As6AvpzX9elz
UmrLydX1cyi4bskqPz5JvpJ/Ulm1VGOIi9BY10lY9mgX+UtBlSUYzXFqwsM+kI/m
nwBfcnfLY5SofFT5ZEG0Gq2aVFq4La0moF+6Em1WWrXEaPLGvNULETlNRCIoE33E
1LuctKU7yeQw1Xyom9t48sX63Ts53CGkosn+IbXJDlmUo7yF4PUEkv3UMUTsMiqF
uJyQMlg6IRwKAHowEt/JhC4sPy4hjdBVbDoXyHH6nfCgUyfekB+3lAOx+3We+UlL
ymmrRQ1dKPUhn+gpeG1q27vcG8TOsN4QUTYCYst7zLALdUCWrMEESGPupVOdvwnR
cXZmfC92doD4R6atK1s2X0QeDIbkvTqY0w==
=HJN9
-----END PGP SIGNATURE-----
